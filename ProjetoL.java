package sample;

import robocode.*;
import java.awt.*;


public class ProjetoL extends AdvancedRobot {
    int moveDirection=1;
	//int turnDirection=1;
   
    public void run() {
        setAdjustRadarForRobotTurn(true);
        setBodyColor(Color.yellow);
        setGunColor(Color.yellow);
        setRadarColor(Color.yellow);
        setScanColor(Color.red);
        setBulletColor(Color.yellow);
        setAdjustGunForRobotTurn(true); 
        turnRadarRightRadians(Double.POSITIVE_INFINITY);
    }

   
    public void onScannedRobot(ScannedRobotEvent e) {
        double absBearing=e.getBearingRadians()+getHeadingRadians();
        double latVel=e.getVelocity() * Math.sin(e.getHeadingRadians() -absBearing);
        double gunTurnAmt;
        setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
        if(Math.random()>.9){
            setMaxVelocity((12*Math.random())+12);
        }
        if (e.getDistance() > 190) {
            gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/22);
            setTurnGunRightRadians(gunTurnAmt);
            setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing-getHeadingRadians()+latVel/getVelocity()));
            setAhead((e.getDistance() - 180)*moveDirection);
            setFire(3);
        }
        else{
            gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/15);
            setTurnGunRightRadians(gunTurnAmt);
            setTurnLeft(-90-e.getBearing());
            setAhead((e.getDistance() - 150)*moveDirection);
            setFire(3);
        }
    }
    public void onHitWall(HitWallEvent e){   
	moveDirection=-moveDirection;
    }
	public void OnHitRobot(HitRobotEvent e)
{   
turnRight(e.getBearing());
if(e.getEnergy()>16) {
	fire(3);
} else if (e.getEnergy() > 10) {
fire(2);
} else if (e.getEnergy() > 4) {
fire(1);
} else if (e.getEnergy() > 2) {
fire(.5);
} else if (e.getEnergy() > .4) {
fire(.1);
}

}
    
}
